/*
 * $ npm install gulp gulp-sass gulp-changed gulp-autoprefixer gulp-cssnano gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload del --save-dev
 */

// set paths
var paths = {
    bower: {
        src: 'resources/assets/bower/'
    },
    styles: {
        src: 'resources/assets/sass/',
        dest: 'public/css/'
    },
    scripts: {
        src: 'resources/assets/js/',
        dest: 'public/js/'
    },
    images: {
        src: 'resources/assets/img/',
        dest: 'public/img/'
    },
    views: {
        src: 'resources/views/'
    },
    app: {
        src: 'app/'
    }
};

// variables
var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

// load plugins
var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    autoprefixer    = require('gulp-autoprefixer'),
    cssnano         = require('gulp-cssnano'),
    uglify          = require('gulp-uglify'),
    imagemin        = require('gulp-imagemin'),
    rename          = require('gulp-rename'),
    concat          = require('gulp-concat'),
    changed         = require('gulp-changed'),
    notify          = require('gulp-notify'),
    livereload      = require('gulp-livereload'),
    del             = require('del');


// styles

// public
gulp.task('style-public', function() {
    gulp.src(paths.styles.src + 'public.scss')
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(notify({ message: 'Public style task complete' }));
});

// admin
gulp.task('style-admin', function() {
    gulp.src(paths.styles.src + 'admin.scss')
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(notify({ message: 'Admin style task complete' }));
});

// scripts

// public
gulp.task('script-public', function() {
    gulp.src([paths.scripts.src + 'public/**/*.js', '!' + paths.scripts.src + 'admin/**/*.js'])
        .pipe(concat('public.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(notify({ message: 'Public script task complete' }));
});

// admin
gulp.task('script-admin', function() {
    gulp.src([paths.scripts.src + 'admin/**/*.js', '!' + paths.scripts.src + 'public/**/*.js'])
        .pipe(concat('admin.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(notify({ message: 'Admin script task complete' }));
});

// vendor scripts
gulp.task('script-vendor', function() {
    gulp.src([
            paths.bower.src + 'jquery/dist/jquery.js',
            paths.bower.src + 'tether/dist/js/tether.js',
            paths.bower.src + 'bootstrap/dist/js/bootstrap.js',
            // paths.scripts.src + 'vendor/html5imageupload.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(notify({ message: 'Vendor script task complete' }));
});

// images
gulp.task('images', function() {
    return gulp.src(paths.images.src + '**/*')
        .pipe(changed(paths.images.dest))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest(paths.images.dest))
        .on('error', console.log)
        .pipe(notify({ message: 'Images task complete' }));
});

// clean
gulp.task('clean', function(cb) {
    del([paths.styles.dest, paths.scripts.dest, paths.images.dest], {force: true}, cb)
});

// default task
gulp.task('default', ['clean', 'style-public', 'style-admin', 'script-vendor', 'script-public', 'script-admin', 'images']);

// watch
gulp.task('watch', function() {

  // watch .scss files
  gulp.watch(paths.styles.src + '**/*.scss', ['style-public', 'style-admin']);

  // watch .js files
  gulp.watch(paths.scripts.src + '**/*.js', ['script-public', 'script-admin', 'script-vendor']);

  // watch image files
  gulp.watch(paths.images.src + '**/*', ['images']);

  // create liveReload server
  livereload.listen();

  // watch any files in public/, reload on change
  gulp.watch([paths.styles.dest + '**', paths.scripts.dest + '**', paths.images.dest + '**', 'app/**', paths.views.src + '**']).on('change', livereload.changed);
});